<!-- 
	Serving PHP Files (run in the terminal):
	php -S localhost:8000
-->

<!-- 
	We will use this method to separate the declaration of variables and functions from the HTML content.

	- "index.php" for embedding PHP in HTML to be served and shown in our browser
	- "code.php" for defining PHP variables, statements, and functions

-->
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: PHP Basics and Selection Control Structure</title>
</head>
<body>
	<!-- Variables can be used to output data in double quotes while single quotes do not -->
	<!-- <h1>Hello World!</h1> -->
	<h1>Echoing Values</h1>
	<p><?php echo "Good day $name! Your given email is $email."; ?> </p>
	<!-- Single quote can be used but concatenation is needed -->
	<!-- dot (.) is used for concatenation -->
	<p><?php echo 'Good day ' . $name .'!'. ' Your given email is '.$email.'.'; ?> </p> 

	<p><?php echo PI; ?></p>

	<p><?php echo "hasTravelledAbroad: $hasTravelledAbroad"; ?></p>
	<p><?php echo "spouse: $spouse"; ?></p>

	<p> <?php echo gettype($hasTravelledAbroad); ?> </p>
	<p> <?= gettype($spouse); ?> </p>

	<!-- 
		-var_dump() function
		-displays structured information about the variable/expressions including its type and values
	-->

	<p><?= var_dump($hasTravelledAbroad)?></p>
	<p><?= var_dump($spouse)?></p>


	<p><?= var_dump($gradesObj); ?></p>


	<p><?php print_r($gradesObj);?></p>


	<!-- To output the value of an object property, the "single arrow" notation can be used -->

	<p><?= $gradesObj->firstGrading; ?></p>
	<p><?= $personObj->address->state; ?></p>


	<!-- Printing the whole array -->
	<p><?= var_dump($grades);?></p>
	<p><?= print_r($grades);?></p>

	<!-- To output an array element, use the usual square bracket -->

	<p><?= $grades[3]; ?></p>
	<p><?= $grades[1]; ?></p>


	<h1>Operators</h1>
	<p>X: <?= $x; ?></p>
	<p>Y: <?= $y; ?></p>

	<p>Is Legal Age: <?= var_dump($isLegalAge); ?></p>
	<p>Is Registered: <?= var_dump($isRegistered); ?></p>


	<h2>Arithmetic Operators</h2>
	<p>Sum: <?= $x+$y?></p>
	<p>Difference: <?= $x-$y?></p>
	<p>Product: <?= $x * $y?></p>
	<p>Quotient: <?= $x / $y?></p>
	<p>Modulo: <?= $x % $y?></p>

	<h2>Equality Operator</h2>
	<p>Loose Equality: <?php echo var_dump($x == '500'); ?></p>
	<p>Strict Equality: <?php echo var_dump($x === '500'); ?></p>
	<p>Loose Inequality: <?php echo var_dump($x != '500'); ?></p>
	<p>Strict Inequality: <?php echo var_dump($x !== '500'); ?></p>

	<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater: <?php echo var_dump($x > $y); ?></p>

	<p>Is Lesser or Equal: <?php echo var_dump($x <= $y); ?></p>
	<p>Is Greater or Equal: <?php echo var_dump($x >= $y); ?></p>


	<h2>Logical Operator</h2>
	<p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are Some Requirements Met: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Are Some Requirements Not Met: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>


	<h1>Function</h1>
	<p>Full Name: <?= getFullName("John", "D.", "Smith"); ?></p>


	<h2>If-Elseif-Else</h2>
	<p> <?= determineTyphoonIntensity(120); ?></p>

	<h2>Ternary Sample (Is Underage)</h2>
	<p>78: <?= var_dump(isUnderAge(78)) ?></p>
	<p>17: <?= var_dump(isUnderAge(17)) ?></p>

	<h2>Switch</h2>

	<p><?php echo determineComputerUser(5); ?></p>
	<p><?php echo determineComputerUser(1); ?></p>

	<h2>Try-Catch-Finally</h2>
	<p><?php greeting("Hello") ?></p>
	<p><?php greeting(25) ?></p>

	</body>
	</html>