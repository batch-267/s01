<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S01: Activity</title>
</head>
<body>
	<h2>Full Address</h2>
	<p><?= getFullAddress("Philippines", "Quezon City", "Metro Manila", "3F Caswynn Bldg., Timog Avenue"); ?></p>
	<p><?= getFullAddress("Philippines", "Makati City", "Metro Manila", "3F Enzo Bldg., Buendia Avenue"); ?></p>

	<h2>Letter-Based Grading</h2>
	<p> <?= getLetterGrade(87) ?></p>
	<p> <?= getLetterGrade(94) ?></p>
	<p> <?= getLetterGrade(74) ?></p>
	<p> <?= getLetterGrade(75) ?></p>
</body>
</html>